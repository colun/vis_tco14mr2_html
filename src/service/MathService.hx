package service;

import math.Int64;
class MathService {
    public static function absInt(x:Int):Int {
        return 0 <= x ? x : -x;
    }

    public static function absInt64(x:Int64):Int64 {
        return 0 <= x ? x : -x;
    }

    public static function maxInt(x:Int, y:Int):Int {
        return x < y ? y : x;
    }

    public static function minInt(x:Int, y:Int):Int {
        return x < y ? x : y;
    }

    public static function parseInt64(str:String):Int64 {
        var neg = false;
        if (str.charAt(0) == "-") {
            str = str.substr(1);
            neg = true;
        }
        var ret:Int64 = Std.parseInt(str.substr(maxInt(0, str.length - 8)));
        if (8 < str.length) {
            var pos1 = str.length - 16;
            var pos2 = maxInt(0, pos1);
            var base:Int64 = 100000000;
            ret = ret + base * Std.parseInt(str.substr(pos2, 8 - (pos2 - pos1)));
            if (16 < str.length) {
                var pos3 = str.length - 24;
                var pos4 = maxInt(0, pos3);
                ret = ret + base * base * Std.parseInt(str.substr(pos4, 8 - (pos4 - pos3)));
            }
        }
        return neg ? -ret : ret;
    }

    public static function mulMod(a:Int, b:Int, MOD:Int):Int {
        var result:Int64 = (Int64.fromInt(a)*b)%MOD;
        return result.toInt();
    }
    public static function invMod(v:Int, MOD:Int):Int {
        var result = Int64.fromInt(1);
        while(v!=1) {
            var u = Std.int(MOD / v);
            var vi = MOD-v*u;
            if(v<vi+vi) {
                v = v-vi;
                result = (result * (u+1)) % MOD;
            }
            else {
                v = vi;
                result = (result * (MOD-u)) % MOD;
            }
        }
        return result.toInt();
    }
    public static function divMod(a:Int, b:Int, MOD:Int):Int {
        return mulMod(a, invMod(b, MOD), MOD);
    }
/*
    @:macro public static function exprInt64(expr:Expr):Expr {
        function typeFunc(expr:Expr):String {
            var type:Type = Context.typeof(expr);
            switch(type) {
                case TInst(t, _):
                    return t.toString();
                case TAbstract(t, _):
                    return t.toString();
                case TType(_, [TAbstract(t, _)]):
                    return t.toString();
                case _:
            }
            trace('${type}');
            return "unknown";
        }
        function func(expr:Expr):Expr {
            switch(expr.expr) {
                case EParenthesis(e):
                    return func(e);
                case EBinop(op, a, b):
                    var aNature = func(a);
                    var bNature = func(b);
                    var typeA = typeFunc(aNature);
                    var typeB = typeFunc(bNature);
                    var aInt64 = aNature;
                    if (typeA == "Int") {
                        aInt64 = macro Int64.ofInt($aNature);
                    }
                    var bInt64 = bNature;
                    if (typeB == "Int") {
                        bInt64 = macro Int64.ofInt($bNature);
                    }
                    var aBool = aNature;
                    if (typeA == "haxe.Int64") {
                        aBool = macro !Int64.isZero($aNature);
                    }
                    var bBool = bNature;
                    if (typeB == "haxe.Int64") {
                        bBool = macro !Int64.isZero($bNature);
                    }
                    if (typeA == "haxe.Int64" || typeB == "haxe.Int64") {
                        switch(op) {
                            case OpAdd:
                                return macro Int64.add($aInt64, $bInt64);
                            case OpMult:
                                return macro Int64.mul($aInt64, $bInt64);
                            case OpDiv:
                                return macro Int64.div($aInt64, $bInt64);
                            case OpSub:
                                return macro Int64.sub($aInt64, $bInt64);
                            case OpAssign:
                                return macro $aInt64 = $bInt64;
                            case OpEq:
                                return macro Int64.compare($aInt64, $bInt64) == 0;
                            case OpNotEq:
                                return macro Int64.compare($aInt64, $bInt64) != 0;
                            case OpGt:
                                return macro Int64.compare($aInt64, $bInt64) > 0;
                            case OpGte:
                                return macro Int64.compare($aInt64, $bInt64) >= 0;
                            case OpLt:
                                return macro Int64.compare($aInt64, $bInt64) < 0;
                            case OpLte:
                                return macro Int64.compare($aInt64, $bInt64) <= 0;
                            case OpAnd:
                                return macro Int64.and($aInt64, $bInt64);
                            case OpOr:
                                return macro Int64.or($aInt64, $bInt64);
                            case OpXor:
                                return macro Int64.xor($aInt64, $bInt64);
                            case OpBoolAnd:
                                return macro $aBool && $bBool;
                            case OpBoolOr:
                                return macro $aBool || $bBool;
                            case OpShl:
                                return macro Int64.shl($aInt64, $bNature);
                            case OpShr:
                                return macro Int64.shr($aInt64, $bNature);
                            case OpUShr:
                                return macro Int64.ushr($aInt64, $bNature);
                            case OpMod:
                                return macro Int64.mod($aInt64, $bInt64);
                            case OpAssignOp(OpAdd):
                                return macro $aInt64 = Int64.add($aInt64, $bInt64);
                            case OpAssignOp(OpMult):
                                return macro $aInt64 = Int64.mul($aInt64, $bInt64);
                            case OpAssignOp(OpDiv):
                                return macro $aInt64 = Int64.div($aInt64, $bInt64);
                            case OpAssignOp(OpSub):
                                return macro $aInt64 = Int64.sub($aInt64, $bInt64);
                            case OpAssignOp(OpAnd):
                                return macro $aInt64 = Int64.and($aInt64, $bInt64);
                            case OpAssignOp(OpOr):
                                return macro $aInt64 = Int64.or($aInt64, $bInt64);
                            case OpAssignOp(OpXor):
                                return macro $aInt64 = Int64.xor($aInt64, $bInt64);
                            case OpAssignOp(_):
                            case OpInterval:
                            case OpArrow:
                        }
                        AssertService.assert(false);
                        return null;
                    }
                    else {
                        return expr;
                    }
                case ECall({expr:EConst(CIdent("abs"))}, arg):
                    AssertService.assert(arg.length == 1);
                    var a:Expr = func(arg[0]);
                    return macro MathService.absInt64($a);
                case ECall({expr:EConst(CIdent("Int64"))}, arg):
                    AssertService.assert(arg.length == 1);
                    var a:Expr = func(arg[0]);
                    var typeA = typeFunc(a);
                    AssertService.assert(typeA != "haxe.Int64");
                    return macro Int64.ofInt($a);
                case _:
                    return expr;
            }
        }
        var exprStack:Array<Expr> = new Array<Expr>();
        var opStack:Array<String> = new Array<String>();
        var result = func(expr);
        var typeR = typeFunc(result);
        if (typeR == "Int") {
            return macro Int64.ofInt($result);
        }
        return result;
    }
    */
}