package;

import service.MathService;
import service.MathService;
import service.MathService;
import gv.Gv;
class Main {
    public static function callback(time:Int, x:Int, y:Int):Void {
        Gv.newTime(time);
        Gv.circle(x, y, 0.1);
        Gv.inputInt(callback);
    }
    static var target:Int = -1;
    static var xList:Array<Int> = new Array<Int>();
    static var yList:Array<Int> = new Array<Int>();
    static var wList:Array<Int> = new Array<Int>();
    static var hList:Array<Int> = new Array<Int>();
    static var cList:Array<Int> = new Array<Int>();
    public static function print() {
        Gv.rollbackAll();
        for(i in 0...xList.length) {
            Gv.rect(xList[i], yList[i], wList[i], hList[i]).color(target==i ? 7 : cList[i]);
        }
    }
    public static function main() {
        xList.push(-10);
        yList.push(-1000);
        wList.push(10);
        hList.push(1000);
        cList.push(cList.length % 7);
        xList.push(-20);
        yList.push(-2000);
        wList.push(10);
        hList.push(1000);
        cList.push(cList.length % 7);
        xList.push(-20);
        yList.push(0);
        wList.push(10);
        hList.push(1000);
        cList.push(cList.length % 7);
        xList.push(-1000);
        yList.push(-1000);
        wList.push(10);
        hList.push(1000);
        cList.push(cList.length % 7);
        var bx = 100;
        var by = 1100;
        var mh = 0;
        while(true) {
            var w = Std.random(801);
            var h = Std.random(801);
            if(w*w+h*h<640000) {
                xList.push(bx);
                yList.push(by);
                mh = Std.int(Math.max(mh, h));
                bx += w + 100;
                if(5000<bx) {
                    bx = 100;
                    by += mh + 100;
                    mh = 0;
                }
                wList.push(w);
                hList.push(h);
                cList.push(cList.length % 7);
                if(49<=wList.length) {
                    break;
                }
            }
        }
        print();
        var beforeX:Int = 0;
        var beforeY:Int = 0;
        Gv.setDragModeInt(function(time:Int, x:Int, y:Int):Void {
            target = -1;
            var bestDiff:Float = 1000000000;
            for(i in 4...xList.length) {
                var diffX = Math.min(Math.abs(xList[i] - x), Math.abs(xList[i] + wList[i] - x));
                if(xList[i]<=x && x<xList[i]+wList[i]) {
                    diffX = 0;
                }
                var diffY = Math.min(Math.abs(yList[i] - y), Math.abs(yList[i] + hList[i] - y));
                if(yList[i]<=y && y<yList[i]+hList[i]) {
                    diffY = 0;
                }
                var diff = diffX * diffX + diffY * diffY;
                if(diff<=bestDiff) {
                    bestDiff = diff;
                    target = i;
                }
            }
            beforeX = x;
            beforeY = y;
            print();
        }, function(x:Int, y:Int):Void {
            Gv.circle(x, y, 0.3);
            if(0<=target) {
                var dx = x - beforeX;
                var dy = y - beforeY;
                if(yList[target]+dy<1000) {
                    for(i in 0...xList.length) {
                        if(i!=target && yList[i]<1000) {
                            var maxSX = MathService.maxInt(xList[target]+dx, xList[i]);
                            var minEX = MathService.minInt(xList[target]+dx+wList[target], xList[i]+wList[i]);
                            if(maxSX < minEX) {
                                var maxSY = MathService.maxInt(yList[target]+dy, yList[i]);
                                var minEY = MathService.minInt(yList[target]+dy+hList[target], yList[i]+hList[i]);
                                if(maxSY < minEY) {
                                    var ddx = 100000;
                                    if(0<dx) {
                                        ddx = (xList[i] - wList[target]) - (xList[target]+dx);
                                    }
                                    else if(dx<0) {
                                        ddx = (xList[i] + wList[i]) - (xList[target]+dx);
                                    }
                                    var ddy = 100000;
                                    if(0<dy) {
                                        ddy = (yList[i] - hList[target]) - (yList[target]+dy);
                                    }
                                    else if(dy<0) {
                                        ddy = (yList[i] + hList[i]) - (yList[target]+dy);
                                    }
                                    if(MathService.absInt(ddx) < MathService.absInt(ddy)) {
                                        dx += ddx;
                                    }
                                    else {
                                        dy += ddy;
                                    }
                                }
                            }
                        }
                    }
                }
                xList[target] += dx;
                yList[target] += dy;
            }
            beforeX = x;
            beforeY = y;
            print();
        }, function():Void {
            target = -1;
        });
        var clickFunc:Int->Int->Int->Void = null;
        clickFunc = function(time:Int, x:Int, y:Int):Void {
            var bestDiff:Float = 1000000000;
            var t:Int = -1;
            for(i in 4...xList.length) {
                var diffX = Math.min(Math.abs(xList[i] - x), Math.abs(xList[i] + wList[i] - x));
                if(xList[i]<=x && x<xList[i]+wList[i]) {
                    diffX = 0;
                }
                var diffY = Math.min(Math.abs(yList[i] - y), Math.abs(yList[i] + hList[i] - y));
                if(yList[i]<=y && y<yList[i]+hList[i]) {
                    diffY = 0;
                }
                var diff = diffX * diffX + diffY * diffY;
                if(diff<=bestDiff) {
                    bestDiff = diff;
                    t = i;
                }
            }
            if(0<=t) {
                var w = wList[t];
                wList[t] = hList[t];
                hList[t] = w;
            }
            print();
            Gv.inputInt(clickFunc);
        };
        Gv.inputInt(clickFunc);
    }
}